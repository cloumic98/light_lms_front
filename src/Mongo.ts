import * as dotenv from 'dotenv';
import {Collection, Db, MongoClient} from "mongodb";
dotenv.config();

export default class Mongo {
    private static INSTANCE: Mongo;
    private mongoClient: MongoClient;
    private mongoDB: Db;

    constructor() {
        this.mongoClient = new MongoClient(process.env.MONGODB_URI!)

        this.mongoClient.connect()
            .then(() => {
                console.log('Connected')
            })
            .catch(console.error)

        this.mongoDB = this.mongoClient.db(process.env.MONGODB_DB!)
    }


    public static get instance() {
        if(Mongo.INSTANCE == null) {
            Mongo.INSTANCE = new Mongo()
        }
        return Mongo.INSTANCE
    }

    public getCollection(name: string): Collection {
        return this.mongoDB.collection(name);
    }
}
