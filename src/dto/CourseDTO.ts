import {IsArray, IsEmail, IsNotEmpty, IsOptional, IsString, ValidateNested} from "class-validator";
import {Type} from "class-transformer";
import "reflect-metadata";

export class CourseInsertDTO {
    @IsString()
    @IsNotEmpty()
    public name!: string

    @IsString()
    @IsNotEmpty()
    public firstname!: string

    @IsString()
    @IsNotEmpty()
    public lastname!: string

    @IsEmail()
    @IsNotEmpty()
    public mail!: string

    @IsArray()
    public students!: []
}

export class CourseModifyDTO {
    @IsString()
    @IsNotEmpty()
    @IsOptional()
    public name!: string

    @IsString()
    @IsNotEmpty()
    @IsOptional()
    public firstname!: string

    @IsString()
    @IsNotEmpty()
    @IsOptional()
    public lastname!: string

    @IsEmail()
    @IsNotEmpty()
    @IsOptional()
    public mail!: string

    @IsArray()
    @IsOptional()
    public students!: []
}
