import {IsArray, IsEmail, IsNotEmpty, IsOptional, IsString} from "class-validator";

export class StudentInsertDTO {
    @IsString()
    @IsNotEmpty()
    public firstname!: string

    @IsString()
    @IsNotEmpty()
    public lastname!: string

    @IsEmail()
    @IsNotEmpty()
    public mail!: string

    @IsArray()
    public evaluation!: []
}

export class StudentModifyDTO {
    @IsString()
    @IsOptional()
    public firstname!: string

    @IsString()
    @IsOptional()
    public lastname!: string

    @IsEmail()
    @IsOptional()
    public mail!: string

    @IsArray()
    @IsOptional()
    public evaluation!: []
}