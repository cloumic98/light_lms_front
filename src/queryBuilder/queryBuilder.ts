import Mongo from "../Mongo";
import {OperatorEnum} from "./operator.enum";
import {ObjectId} from "mongodb";
import {StudentInsertDTO, StudentModifyDTO} from "../dto/StudentDTO";


export class Query {

    private collection: string
    private mainType: 'find' | 'findOne' | 'distinct' | 'removeOne' | 'remove' | 'insert' | 'update' | null = null
    private dataInsert: StudentInsertDTO | any
    private dataUpdate: StudentModifyDTO | any
    private filters: any[]

    constructor() {
        this.collection = ""
        this.filters = []
    }

    public useCollection(collectionName: string): Query {
        this.collection = collectionName
        return this
    }

    public find(): Query {
        this.mainType = 'find'
        return this
    }

    public findOne(): Query {
        this.mainType = 'findOne'
        return this
    }

    public distinct(): Query {
        this.mainType = "distinct"
        return this
    }

    public removeOne(): Query {
        this.mainType = "removeOne"
        return this
    }

    public remove(): Query {
        this.mainType = "remove"
        return this
    }

    public insert(documentData: StudentInsertDTO | any): Query {
        this.mainType = "insert"
        this.dataInsert = documentData
        return this
    }

    public update(documentData: StudentModifyDTO | any): Query {
        this.mainType = "update"
        this.dataUpdate = documentData
        return this
    }

    public addFilter(fieldName: string, value: string | number | ObjectId, operator: OperatorEnum = OperatorEnum.EQUAL): Query {
       this.filters.push({ [fieldName] : operator == OperatorEnum.EQUAL ? value : { [operator] : value } })
        return this
    }

    private groupFilter() {
        let result = this.filters.reduce(function(obj, item) {
            obj[Object.keys(item)[0]] = item[Object.keys(item)[0]];
            return obj;
        }, {});
        return result
    }

    public execute() {
        if(this.mainType == null || this.collection == "") return null;

        const coll = Mongo.instance.getCollection(this.collection)

        const groupedFilter = this.groupFilter()

        switch (this.mainType) {
            case 'find':
                return coll.find(groupedFilter)
            case 'findOne':
                return coll.findOne(groupedFilter)
            case 'removeOne':
                return coll.deleteOne(groupedFilter)
            case 'remove':
                return coll.deleteMany(groupedFilter)
            case 'insert':
                return coll.insertOne(this.dataInsert)
            case 'update':
                return coll.updateOne(groupedFilter, this.dataUpdate)
            default:
                return null
        }
    }
}

