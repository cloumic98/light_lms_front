

export enum OperatorEnum {
    EQUAL = "$eq",
    GREATER = "$gt",
    GREATEROREQUAL = "$gte",
    LESS = "$lt",
    LESSOREQUAL = "$lte",
    UNWIND = "$unwind",
    IN= "$in",
    NOTEQUAL = "$ne",
    NOTIN = "$nin"

    /*
    $eq	Matches values that are equal to the given value.
    $gt	Matches if values are greater than the given value.
    $lt	Matches if values are less than the given value.
    $gte	Matches if values are greater or equal to the given value.
    $lte	Matches if values are less or equal to the given value.
    $in	Matches any of the values in an array.
    $ne	Matches values that are not equal to the given value.
    $nin	Matches none of the values specified in an array.
    */

}
