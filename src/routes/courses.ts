import express from "express";
import {Query} from "../queryBuilder/queryBuilder";
import {DeleteResult, FindCursor, ObjectId, WithId} from "mongodb";
import {makeValidateBody} from "express-class-validator";
import {CourseInsertDTO, CourseModifyDTO} from "../dto/CourseDTO";

export const courseRouter = express.Router()

courseRouter.get('/', async (req, res) => {
    const query = await new Query()
        .useCollection('course')
        .find()
        .execute() as FindCursor<WithId<Document>>;

    res.send(await query!.toArray())
})

courseRouter.get('/:idCourse', async (req, res) => {
    const idCourse = req.params.idCourse

    const query = await new Query()
        .useCollection('course')
        .findOne()
        .addFilter("_id", new ObjectId(idCourse))
        .execute() as FindCursor<WithId<Document>>;

    if(query == null) {
        res.send({ error: "Course not found" }).status(404);
    } else {
        res.send(query);
    }
})

courseRouter.delete('/:idCourse', async (req, res) => {
    const idCourse = req.params.idCourse;

    const query = await new Query()
        .useCollection('course')
        .removeOne()
        .addFilter("_id", new ObjectId(idCourse))
        .execute() as DeleteResult;

    if(query!.deletedCount < 1) {
        res.send({ error: "Course not found" }).status(404)
    } else {
        res.send(query);
    }
})

courseRouter.post('/', makeValidateBody(CourseInsertDTO), async (req, res) => {
    const data = req.body;

    const course = {
        name: data.name,
        students: data.students,
        trainer: {
            firstName: data.firstname,
            lastname: data.lastname,
            mail: data.mail
        }
    }

    const query = await new Query()
        .useCollection('course')
        .insert(course)
        .execute();

    res.send(query)
})

courseRouter.put('/:idCourse', makeValidateBody(CourseModifyDTO), async (req, res) => {
    const idCourse = req.params.idCourse;

    const query = await new Query()
        .useCollection('course')
        .update({
            $set: req.body
        })
        .addFilter("_id", idCourse)
        .execute();

    res.send(query)
})

courseRouter.post('/filter', async (req, res) => {
    const query = new Query()
        .useCollection('course')
        .find()

    const body = req.body

    body.filters.forEach((filter: any) => {
        query.addFilter(filter.column, filter.value, filter.operator)
    })

    const queryResult = await query.execute() as FindCursor<WithId<Document>>;

    res.send(await queryResult!.toArray());
})

courseRouter.get('/stats/course', async (req, res) => {
    const result = {
        count: 0
    }

    const query = await new Query()
        .useCollection('course')
        .find()
        .execute() as FindCursor<WithId<Document>>;

    result.count = await query.count()

    res.send(result)
})
