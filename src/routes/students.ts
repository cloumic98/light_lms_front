import express from "express";
import {DeleteResult, FindCursor, ObjectId, WithId} from "mongodb";
import {Query} from "../queryBuilder/queryBuilder";
import {makeValidateBody} from "express-class-validator";
import {StudentInsertDTO, StudentModifyDTO} from "../dto/StudentDTO";
import Mongo from "../Mongo";

export const studentRouter = express.Router()

studentRouter.get('/', async (req, res) => {
    const query = await new Query()
        .useCollection('student')
        .find()
        .execute() as FindCursor<WithId<Document>>;

    res.send(await query!.toArray());
})

studentRouter.get('/:idStudent', async (req, res) => {
    const idStudent = req.params.idStudent;

    const query = await new Query()
        .useCollection('student')
        .findOne()
        .addFilter("_id", new ObjectId(idStudent))
        .execute();

    if(query == null) {
        res.send({ error: "Student not found" }).status(404);
    } else {
        res.send(query);
    }
})

studentRouter.delete('/:idStudent', async (req, res) => {
    const idStudent = req.params.idStudent;

    const query = await new Query()
        .useCollection('student')
        .removeOne()
        .addFilter("_id", new ObjectId(idStudent))
        .execute() as DeleteResult;

    if(query!.deletedCount < 1) {
        res.send({ error: "Student not found" }).status(404)
    } else {
        res.send(query);
    }
})


studentRouter.post('/', makeValidateBody(StudentInsertDTO), async (req, res) => {
    const data = req.body;

    const query = await new Query()
        .useCollection('student')
        .insert(data)
        .execute();

    res.send(query)
})

studentRouter.put('/:idStudent', makeValidateBody(StudentModifyDTO), async (req, res) => {
    const idStudent = req.params.idStudent;

    const query = await new Query()
        .useCollection('student')
        .update({
            $set: req.body
        })
        .addFilter("_id", idStudent)
        .execute();

    res.send(query)
})

studentRouter.post('/filter', async (req, res) => {
    const query = new Query()
        .useCollection('student')
        .find()

    const body = req.body

    body.filters.forEach((filter: any) => {
        query.addFilter(filter.column, filter.value, filter.operator)
    })

    const queryResult = await query.execute() as FindCursor<WithId<Document>>;

    res.send(await queryResult!.toArray());
})

studentRouter.get('/stats/student', async (req, res) => {
    const result = {
        count: undefined,
        moyenne: undefined
    }

    const query = await new Query()
        .useCollection('student')
        .find()
        .execute() as FindCursor<WithId<Document>>;

    // @ts-ignore
    result.count = await query.count()

    // @ts-ignore
    result.moyenne = await Mongo.instance.getCollection("student").aggregate([
        { $unwind: "$evaluation" },
        { $group: {
            _id: "$_id",
            moyenne: { $avg: "$evaluation.note" }
        }}
    ]).toArray()

    res.send(result)
})
