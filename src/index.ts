import express from 'express';
import cors from 'cors';
import Mongo from "./Mongo";
import {courseRouter} from "./routes/courses";
import {studentRouter} from "./routes/students";

const app = express();

app.use(cors())
app.use(express.json())

app.use('/students', studentRouter)
app.use('/courses', courseRouter)

app.listen(3000, () => {
    Mongo.instance
})

